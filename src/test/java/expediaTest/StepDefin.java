package expediaTest;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import expediaBase.Basepage;
import expediaPage.Homepage;
import expediaPage.Searchcarpage;
import expediaPage.Signingpage;
import org.openqa.selenium.WebDriver;

/**
 * Created by User on 22/04/2017.
 */
public class StepDefin extends Basepage {
    public WebDriver driver;

         public StepDefin(){
             this.driver = HOOK.driver;
         }

    @Given("^That am on the web page$")
    public void that_am_on_the_web_page() throws Throwable {
        driver.get("https://www.expedia.co.uk");
        Homepage Hp = new Homepage(driver);
        Hp.LogIn();
        System.out.println("MOYO JUST JOIN THE TEAM");


    }

    @Then("^Am able to log in as  \"([^\"]*)\" and \"([^\"]*)\" with login details$")
    public void am_able_to_log_in_as_and_with_login_details(String Uname, String Pwd) throws Throwable {
        Signingpage Lp = new Signingpage(driver);
        Lp.ChechIn(Uname,Pwd);
        System.out.println("TRAIN PAGE ADDED");

    }

    @When("^I am able to search for car to my destination$")
    public void i_am_able_to_search_for_car_to_my_destination() throws Throwable {
        Searchcarpage Sp = new Searchcarpage(driver);
        Sp.SearchForCar();

    }

    @When("^Once i click on search am presented with a choice of cars from different operator$")
    public void once_i_click_on_search_am_presented_with_a_choice_of_cars_from_different_operator() throws Throwable {
        Searchcarpage Sp = new Searchcarpage(driver);
        Sp.SearchForCar();
    }

          @Then("^Log out from account$")
        public void log_out_from_account() throws Throwable {

    }


}
