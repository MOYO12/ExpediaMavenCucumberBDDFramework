package expediaTest;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by User on 22/04/2017.
 */




@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resource",
        plugin = {"pretty","html:target/cucumber", "json:target/cucumber/report.json"},
        tags = "@EMMY",
        monochrome = false
)
public class TestRunner {
}
