package expediaPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by User on 23/04/2017.
 */
public class Signingpage {
    public WebDriver driver;

            public Signingpage(WebDriver driver){
                this.driver=driver;
                PageFactory.initElements(driver,this);
            }
    @FindBy(how = How.ID,using = "signin-loginid")public static WebElement Email;
    @FindBy(how = How.ID,using = "signin-password")public static WebElement Paasw;
    @FindBy(how = How.ID,using = "submitButton")public static WebElement Button;



    public void ChechIn(String Uname, String Pwd){
        Email.sendKeys(Uname);
        Paasw.sendKeys(Pwd);
        Button.click();

    }

}
