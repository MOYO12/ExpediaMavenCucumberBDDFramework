package expediaPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by User on 22/04/2017.
 */
public class Homepage {
    public WebDriver driver;

              public Homepage(WebDriver driver){
                  this.driver=driver;
                  PageFactory.initElements(driver,this);
        }
    @FindBy(how = How.ID,using = "header-account-menu")public static WebElement Account;
    @FindBy(how = How.ID,using = "header-account-signin-button")public static WebElement SignIn;



    public void LogIn(){
        Account.click();
        SignIn.click();
    }
}
