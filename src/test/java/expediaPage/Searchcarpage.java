package expediaPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by User on 23/04/2017.
 */
public class Searchcarpage {

    public WebDriver driver;

             public Searchcarpage(WebDriver driver){
                 this.driver=driver;
                 PageFactory.initElements(driver,this);
             }
    @FindBy(how = How.ID,using = "primary-header-car")public static WebElement Car;
    @FindBy(how = How.ID,using = "car-pickup-clp")public static WebElement PickUp;
    @FindBy(how = How.ID,using = "car-dropoff-clp")public static WebElement DropOff;
    @FindBy(how = How.ID,using = "car-pickup-date-clp")public static WebElement PickUpDate;
    @FindBy(how = How.ID,using = "car-dropoff-date-clp")public static WebElement Clear;
    @FindBy(how = How.ID,using = "car-pickup-time-clp")public static WebElement PickUpTime;
    @FindBy(how = How.ID,using = "car-dropoff-date-clp")public static WebElement DropOffDate;
    @FindBy(how = How.ID,using = "car-dropoff-time-clp")public static WebElement DropOffTime;
    @FindBy(how = How.ID,using = "driver-age-checkbox-clp")public static WebElement DriverAge;
    @FindBy(how = How.XPATH,using = "//button[@type='submit']")
    public static WebElement Search;




    public void SearchForCar() throws InterruptedException {
        Car.click();
        PickUp.sendKeys("Canary Wharf, London, England, United Kingdom");
        DropOff.sendKeys("Knightsbridge, London, England, United Kingdom");
        PickUpDate.sendKeys("22/04/2017");
        PickUpTime.click();
        Select sel = new Select(PickUpTime);
        sel.selectByValue("1130AM");
        Clear.clear();
        DropOffDate.sendKeys("13/05/2017");
        Select aud = new Select(DropOffTime);
        aud.selectByValue("0130AM");
        DriverAge.click();
        Search.click();
    }

}
